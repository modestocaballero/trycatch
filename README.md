# TryCatch #

Create a vhost and point it to src/2_architectural_struggle
You can launch phpunit tests from root by doing:

```
#!shellscript

 phpunit -v --log-junit=/tmp/logjunit.xml --coverage-xml=/tmp/coverage.xml --bootstrap=src/autoload.php --stderr test/2_architectural_struggle/app/
```

There's a sql file on app/install, which can be used to populate DB for both /src and /test