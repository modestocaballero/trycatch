<?php
/**
 * Solver for Multiplier class.
 * Gets sums of multiples under a maximum value, and helper functions.
 */

namespace multiples;

class Solver {

  /**
   * Gets the sum of multiples of the given values under a given maximum.
   *
   * @param array $values Array with the value to get multiples from.
   * @param $max Max value multiples can't get higher than.
   * @return int Sum of multiples of given values.
   */
  function sumMultiplesUnderMax(array $values, $max) {
    // Initializing result variable.
    $sum = 0;
    // Setting the same amount for every value and its key.
    $multipliers = array_combine($values, $values);
    // Turning every value into a Multiplier for that given value.
    $multipliers = array_map(
      function ($value) {
        return new Multiplier($value);
      }, $multipliers);
    // Keep on while there are multipliers to get multiples from.
    while (!empty($multipliers)) {
      foreach ($multipliers as $value => &$multiplier) {
        // If current multiple is lower than max, let's sum it to total.
        if ($multiplier->currentMultiple() < $max) {
          $sum += $multiplier->currentMultiple();
          $multiplier->next();
        }
        // Otherwise, this and higher values are not interesting anymore,
        // we can remove it from our list and pass to the next one.
        else {
          unset($multipliers[$value]);
        }
      }
    }
    return $sum;
  }

  /**
   * Faster version for the same result as sumMultiplesUnderMax.
   * It gets the sum of multiples for every value using a faster formula
   * (sumMultiplesOneValueUnderMax) and adds them up.
   *
   * @param array $values Array with the value to get multiples from.
   * @param $max Max value multiples can't get higher than.
   * @return int Sum of multiples of given values.
   */
  function sumMultiplesUnderMaxFaster(array $values, $max) {
    $sum = 0;
    foreach ($values as $value) {
      // Getting sum of multiples under max for every value.
      $sum += $this->sumMultiplesOneValueUnderMax($value, $max);
    }
    return $sum;
  }

  /**
   * Gets the sum of common multiples for all values provided, under $max
   * @param array $values
   * @param $max
   * @return int Sum of common multiples.
   */
  function sumCommonMultiplesUnderMax(array $values, $max) {
    $commonMultiple = $this->commonMultiple($values);
    return $this->sumMultiplesUnderMaxFaster(array($commonMultiple), $max);
  }

  /**
   * Given an array with values, removes duplicates and multiplies all of them.
   * Note: I know it would be better if we got "Least common multiple",
   * but this solution is good enough for the values used on the problem.
   * @param $values
   */
  function commonMultiple($values) {
    // remove duplicates
    $values = array_unique($values);
    $result = 1;
    foreach ($values as $value) {
      $result *= $value;
    }
    return $result;
  }

  /**
   * Gets the sum of multiples of value under max,
   * using the formula: Sn = (n/2) * (a1 + an)
   * @param $value Value we'll get multiples from.
   * @param $max All multiples must be lower than this one.
   * @return int Sum of multiples.
   */
  function sumMultiplesOneValueUnderMax($value, $max){
    $multiplier = new Multiplier($value);
    // Value is the first element to be added.
    $bottom = $value;
    // And top value is the highest multiple of value
    // which is lower than max
    $top = $multiplier->highestMultipleUnder($max);
    // Since bottom is value * n, we can get n:
    $n = $top / $bottom;
    // And the formula:
    return (int) (($n / 2) * ($bottom + $top));
  }
} 