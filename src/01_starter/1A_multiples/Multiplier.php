<?php
/**
 * Multiplier class
 */

namespace multiples;


class Multiplier {
  // Value we'll increase or decrease to get succesives multiples.
  private $value;
  // Current multiple.
  private $currentMultiple;

  public function __construct($value, $currentMultiple = 0) {
    $this->value = $value;
    $this->currentMultiple = $currentMultiple > 0 ? $currentMultiple : $value;
  }

  /**
   * Returns Multiplier's value.
   * @return int Multiplier's value.
   */
  public function value() {
    return $this->value;
  }

  /**
   * Returns current multiple.
   * @return int Current multiple.
   */
  public function currentMultiple() {
    return $this->currentMultiple;
  }

  /**
   * Increases current multiple to the next multiple.
   */
  public function next() {
    $this->currentMultiple += $this->value;
  }

  /**
   * Decreases current multiple to the previous multiple.
   * If current multiple was lower than value, it gets set to value.
   */
  public function prev() {
    $this->currentMultiple -= $this->value;
    if ($this->currentMultiple < $this->value) {
      $this->currentMultiple = $this->value;
    }
  }

  /**
   * Returns highest multiple which is lower than a given integer.
   *
   * @param int $max
   * @return int A multiple of this Multiplier which is lower than max.
   */
  public function highestMultipleUnder($max){
    // We'll divide max by value, and we'll substract remainder to max.
    $division = $max / $this->value();
    $remainder = $division - floor($division);

    // If remainder is zero, $max is multiple of $value,
    // so lowest multiple is the previous one.
    if ($remainder == 0) {
      return $max - $this->value();
    }
    // Otherwise, substract the reminder from max.
    return (int) ($max - ($remainder * $this->value()));
  }
} 