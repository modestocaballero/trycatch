<?php
/**
 * Math Class.
 *
 * Includes custom functions for multiplication and power.
 */

namespace power;

class Math {

  /**
   * Custom function to multiply two numbers by using repeated sums.
   * @return int Product of two numbers.
   */
    function multiply($a, $b) {
    // Since $a controls the number of iterations,
    // we want it to be the lowest possible.
    // And since $a * $b = $b * a, we swap them if that happens.
    if ($a > $b) {
      list($a, $b) = array($b, $a);
    }
    $result = 0;
    for ($i = 1; $i <= $a; $i++) {
      $result += $b;
    }
    return $result;
  }

  /**
   * Custom function for exponentiation. Calculates a^b.
   * @return int
   */
  function power($a, $b){
    $result = 1;

    for ($i = $b; $i > 0; $i--) {
      $result = $this->multiply($result, $a);
    }
    return $result;
  }
} 