<?php
/**
 * Fibonacci class.
 */

namespace fibonacci;

class Fibonacci {
  public static function fibonacciNonRecursive($n) {
    $result = array();
    for ($i = $n; $i > 0; $i--) {
      $current = (!empty($prev) && !empty($prev2)) ? $prev + $prev2 : 1;
      $result[] = $current;
      $prev2 = !empty($prev) ? $prev : 0;
      $prev = $current;
    }
    return $result;
  }
  public static function fibonacciRecursive($n) {
    // Base case: Fibonacci item 1 is 1.
    if ($n == 1) {
      return array(1);
    }
    // If not, we'll ask for the previous one…
    $previousFibonacci = self::fibonacciRecursive($n - 1);
    // Our result will be the previous one plus one item,
    // so we can copy the previous one as-is.
    $resultArray = $previousFibonacci;
    // Our previous item is at the end of previous fibonacci.
    $prev = end($previousFibonacci);
    // And the previous will be before that, if it exists.
    // If it doesn't, we'll just add zero.
    // prev() moves the array iterator back one position,
    // so current() will give us the item that prev() found, or FALSE.
    $prev2 = prev($previousFibonacci) ? current($previousFibonacci) : 0;
    // And current item is the sum of the two previous ones.
    $resultArray[] = $prev + $prev2;
    return $resultArray;
  }
}