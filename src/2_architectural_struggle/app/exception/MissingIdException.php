<?php
/**
 * @file
 * Class MissingIdException
 * Extends Exception to alert about missing ids.
 *
 * @author Modesto Caballero <hola@modestocaballero.com>
 *
 * PHP Version 5.3.0
 */
namespace app\exception;

/**
 * Class MissingIdException
 * Extends Exception to alert about missing ids.
 */
class MissingIdException extends \Exception {

} 