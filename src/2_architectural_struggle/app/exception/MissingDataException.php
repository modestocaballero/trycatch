<?php
/**
 * @file
 * Class MissingDataException
 * Extends Exception to alert about missing data.
 *
 * @author Modesto Caballero <hola@modestocaballero.com>
 *
 * PHP Version 5.3.0
 */

namespace app\exception;

/**
 * Class MissingDataException
 * Extends Exception to alert about missing data.
 */
class MissingDataException extends \Exception{

} 