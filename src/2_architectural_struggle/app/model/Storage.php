<?php
/**
 * @file
 * Class Storage
 * Abstract class that provides changeable subclasses for storage.
 *
 * @author Modesto Caballero <hola@modestocaballero.com>
 *
 * PHP Version 5.3.0
 */

namespace app\model;

/**
 * Class Storage
 * Abstract class that provides changeable subclasses for storage.
 */
abstract class Storage
{
    // includes all created instances so we can reuse them if needed.
    protected static $instances;

    /**
     * Provides a subclass instance according to type and parameters.
     *
     * @param string $type  Type of Storage we want to use: csv, mysql.
     * @param array  $args  Array with arguments to be used.
     * @param bool   $clear If we want to delete existing cached instance.
     *
     * @return mixed Storage instance.
     */
    public static function getInstance($type, $args = array(), $clear = false)
    {
        switch ($type) {
        case 'csv':
            $path = $args[0];
            $headers = !empty($args[1]) ? $args[1] : array();
            // if instance already exist it, reuse it.
            // unless we want to clear it.
            // We can save multiple csv Storages if needed
            // (they are stored with file path as a key).
            if (empty(self::$instances['csv'][$path]) || $clear) {
                self::$instances['csv'][$path] = new StorageCSV($path, $headers);
            }
            return self::$instances['csv'][$path];
        case 'mysql':
            if (empty(self::$instances['mysql']) || $clear) {
                self::$instances['mysql'] = new StorageMySQL($args);
            }
            return self::$instances['mysql'];
        default:
            throw new \Exception("Storage class doesn't exist.");
        }
    }

    /**
     * Loads a single entry.
     *
     * @param mixed $id Id for the entry we want.
     *
     * @return array The entry you want.
     */
    abstract public function load($id);

}