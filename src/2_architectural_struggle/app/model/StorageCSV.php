<?php
/**
 * @file
 * Class StorageCSV
 * Storage subclass, provides storage via CSV.
 *
 * @author Modesto Caballero <hola@modestocaballero.com>
 *
 * PHP Version 5.3.0
 */

namespace app\model;

/**
 * Class StorageCSV
 * Storage subclass, provides storage via CSV.
 */
class StorageCSV extends Storage
{
    protected $entries = array();
    protected $path;
    protected $headers;

    /**
     * Constructor for StorageCSV
     *
     * @param string $path    Path for CSV file.
     * @param array  $headers array with headers
     *  for being used as keys in result arrays.
     */
    protected function __construct($path, $headers = array())
    {
        $this->path = $path;
        $this->headers = $headers;
        // Let's parse csv here,
        // so all rows will be available from the beginning.
        $this->parseCSV();
    }

    /**
     * Gets all entries from file into $this->entries.
     *
     * @return bool true if operation was successful, false otherwise.
     *
     * @throws \Exception if file can't be successfully opened.
     */
    protected function parseCSV()
    {
        if (empty($this->entries)) {
            // Making sure path exists an file can be read.
            if (file_exists($this->path) && $file = fopen($this->path, 'r')) {
                while (($line = fgetcsv($file)) !== false) {
                    // If headers are present,
                    // we'll use them as keys for our $results.
                    if (!empty($this->headers)) {
                        $this->entries[] = array_combine(
                            $this->headers,
                            $line
                        );
                    } else {
                        // If not, standard numeric keys will be used.
                        $this->entries[] = $line;
                    }
                }
                fclose($file);
            } else {
                throw new \Exception('File path is not valid.');
            }
        }
        return true;
    }

    /**
     * Gets a single entry.
     *
     * @param int $id the identifier for the entry you want.
     *
     * @return array the entry you've asked for if it exists, false otherwise.
     *
     */

    public function load($id)
    {
        return !empty($this->entries[$id]) ? $this->entries[$id] : false;
    }

} 