<?php
/**
 * @file
 * Class AddressModel
 * Model for Address.
 *
 * @author Modesto Caballero <hola@modestocaballero.com>
 *
 * PHP Version 5.3.0
 */

namespace app\model;

/**
 * Class AddressModel
 * Model for Addresses.
 */
class AddressModel extends Model
{
    protected $storage;

    /**
     * Address Model constructor.
     *
     * It loads all addresses from storage.
     */
    public function __construct(\app\Model\Storage $storage)
    {
        $this->storage = $storage;
    }

    public function createAddress($data)
    {
        return $this->storage->create($data);
    }

    public function updateAddress($id, $data)
    {
        return $this->storage->update($id, $data);
    }

    /**
     * Gets a single address.
     *
     * @param int $id the identifier for the address you want.
     *
     * @return array the address you've asked for if it exists, false otherwise.
     *
     */
    public function getAddress($id)
    {
        // Storage will return the entry, or false if it doesn't exist.
        // So we can trust in Storage, and return what it returns.
        return $this->storage->load($id);
    }

    public function deleteAddress($id)
    {
        return $this->storage->delete($id);
    }
}