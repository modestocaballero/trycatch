<?php
/**
 * @file
 * Class StorageMySQL
 * Provides Storage via MySQL.
 *
 * @author Modesto Caballero <hola@modestocaballero.com>
 *
 * PHP Version 5.3.0
 */

namespace app\model;

/**
 * Class StorageMySQL
 * Provides Storage via MySQL.
 */
class StorageMySQL extends \app\model\Storage
{
    protected $connection;
    protected $table;
    /**
     *
     */
    protected function __construct($args = array())
    {
        // Checking every parameter is there and it's not empty.
        // Pass can be empty, we'll not check if it's there or empty.
        $argsKeys = array('host', 'db', 'user');
        foreach ($argsKeys as $key) {
            if (!array_key_exists($key, $args) || empty($args[$key])) {
                throw new \Exception("StorageMySQL: missing {$key} argument");
            }
        }
        // if pass was not there, let's add an empty one.
        $args['pass'] = !empty($args['pass']) ? !empty($args['pass']) : '';
        try {
            $this->connection = new \PDO(
                "mysql:host={$args['host']};dbname={$args['db']}",
                $args['user'], $args['pass']
            );
            $this->connection->setAttribute(
                \PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION
            );
        } catch(PDOException $e) {
            echo 'There was an error connecting to the database. The message is: ' . $e->getMessage();
        }

        if (!empty($args['table']) || $this->tableExists($args['table']))
        {
            $this->table = $args['table'];
        } else {
            throw new \Exception("StorageMySQL: missing '{$args['table']}' table");
        }
    }

    public function create($data)
    {
        try {
            $stmt = $this->connection->prepare(
                "INSERT INTO {$this->table} (name, phone, street) VALUES(:name, :phone, :street)"
            );
            return
                $stmt->execute(
                    array(
                        ':name' => $data['name'],
                        ':phone' => $data['phone'],
                        ':street' => $data['street'],
                    )
                );
        } catch(PDOException $e) {
            // We don't want to get too specific on SQL errors.
            return false;
        }
    }

    function update($id, $data)
    {
        try {
            $stmt = $this->connection->prepare(
                "UPDATE {$this->table} SET name = :name, phone = :phone, street = :street WHERE id = :id"
            );
            return $stmt->execute(
                array(
                    ':name' => $data['name'],
                    ':phone' => $data['phone'],
                    ':street' => $data['street'],
                    ':id' => $id,
                )
            );
        } catch(PDOException $e) {
            // We don't want to get too specific on SQL errors.
            return false;
        }
    }

    /**
     * Loads a single entry.
     *
     * @param mixed $id Id for the entry we want.
     *
     * @return array|void Entry if successful, false otherwise.
     */
    public function load($id)
    {
        try {
            $stmt = $this->connection->prepare(
                "SELECT * FROM {$this->table} WHERE id = :id"
            );
            $stmt->execute(array(':id' => $id));

            while ($result = $stmt->fetch(\PDO::FETCH_ASSOC)) {
                $output = $result;
            }
            return !empty($output) ? $output : false;
        } catch(PDOException $e) {
            // We don't want to get too specific on SQL errors.
            return false;
        }
    }

    /**
     * Deletes a single entry.
     *
     * @param int $id Id for the entry we want to delete.
     *
     * @return bool true if successful, false otherwise.
     */
    public function delete($id)
    {
        try {
            $stmt = $this->connection->prepare("DELETE FROM {$this->table} WHERE id = :id");
            $stmt->execute(array('id' => $id));
            return true;
        } catch(PDOException $e) {
            // We don't want to get too specific on SQL errors.
            return false;
        }
    }

    /**
     * Tells if a table exists in current database.
     *
     * @param string $tableName Table to look for
     *
     * @return bool True if it exists, false otherwise.
     *
     */
    protected function tableExists($tableName)
    {
        $result = $this->connection->query("SHOW TABLES LIKE '{$tableName}'");
        return ($result->rowCount()) > 0;
    }
}