<?php
/**
 * @file
 * Controller class.
 * Parent class for all controllers in this app.
 *
 * @author Modesto Caballero <hola@modestocaballero.com>
 *
 * PHP Version 5.3.0
 */

namespace app\controller;

/**
 * Class Controller
 * Parent class for all controllers in this app.
 */
class Controller
{
    protected $model;

    /**
     * Constructor for Model.
     *
     * @param Model $model The model to be used by this class.
     */
    public function __construct(\app\model\Model $model)
    {
        $this->model = $model;
    }

    public static function decodeInput($data)
    {
        $type = self::getContentType();
        switch ($type) {
        case 'json':
            // We've been using arrays everywhere else until now,
            // so we'll keep it this way.
            return (array) json_decode($data);
        }
    }


    public static function getContentType() {
        // Getting value for Content-Type header.
        // It comes in the form application/json; charset=UTF-8
        // So we'll use explode and get the fist element.
        $contentType = explode(';', $_SERVER['CONTENT_TYPE']);
        // There should be no blanks, but just in case…
        $contentType = trim(array_shift($contentType));
        // Currently we're only detecting json,
        // but in the future we could be detecting other formats.
        switch($contentType) {
        case 'application/json':
        default:
            // JSON is also the fallback so default is using this code, too.
            $output = 'json';
        }
        return $output;
    }

    /**
     * Parses url and returns operation and arguments.
     *
     * @return array Array with the keys:
     *  'op' => includes the operation to be performed.
     *  'args' => array with arguments for operation.
     */
    public static function urlParse() {
        $verb = strtolower($_SERVER['REQUEST_METHOD']);
        // .htaccess has moved all url to $_GET['q'].
        // We don't need trailing slash, we can remove it.
        $path = rtrim($_GET['q'], '/');
        $path = filter_var($path, FILTER_SANITIZE_URL);
        $path = explode('/', $path);
        return array(
            'verb' => $verb,
            // we extract the first element
            'noun' => array_shift($path),
            // the rest of the array elements are arguments.
            'args' => !empty($path) ? $path : array(),
        );
    }
}