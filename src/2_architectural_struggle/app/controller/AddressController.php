<?php
/**
 * @file
 * Class AddressController
 *
 * @author Modesto Caballero <hola@modestocaballero.com>
 *
 * PHP Version 5.3.0
 */

namespace app\controller;
use \app\exception\MissingIdException;
use \app\exception\MissingDataException;
use \app\lib\Response;

/**
 * Class AddressController
 *
 * Controller for Address.
 */
class AddressController extends Controller
{
    public function __construct($model)
    {
        parent::__construct($model);
    }

    public function start()
    {
        $path = self::urlParse();
        try {
            switch ($path['verb']) {
            case 'post':
                // create
                // extracting data from request.
                $data = $this->getDataFromRequest();
                echo $this->createAddress($data);
                break;
            case 'get':
                // read
                $id = AddressController::getId($path);
                echo $this->getAddress($id);
                break;
            case 'put':
                // update
                $data = $this->getDataFromRequest();
                $id = AddressController::getId($path);
                echo $this->updateAddress($id, $data);
                break;
            case 'delete':
                // delete
                $id = AddressController::getId($path);
                echo $this->deleteAddress($id);
                break;
            default:
                header('HTTP/1.1 405 Method Not Allowed');
                header('Allow: GET, PUT, POST, DELETE');
                break;
            }
        } catch (MissingDataException $e){
            self::returnError('Data is empty or not valid.');
        } catch (MissingIdException $e){
            self::returnError('ID is Missing or Wrong');
        }
    }

    /**
     * Returns a successful message using Response class.
     *
     * @param $data Data to be returned: an Address or a message.
     */
    static public function returnSuccess($data)
    {
        $response = new Response();
        $response->setData($data);
        $response->setSuccessful();
        return $response->toJSON();
    }

    /**
     * Returns a successful message using Response class.
     *
     * @param string $message Message to be returned.
     */
    static public function returnError($message)
    {
        $response = new Response();
        $response->setSuccessful(false, $message);
        return $response->toJSON();
    }

    protected static function getDataFromRequest()
    {
        if ($data = file_get_contents("php://input"))
        {
            $data = self::decodeInput($data);
            if (self::validateInput($data)) {
                return $data;
            }
        }
        throw new MissingDataException('Data is empty or not valid.');
    }

    /**
     * Gets id from path array.
     *
     * @param $path Array provided by parseUrl
     *
     * @return int Numeric Id.
     * @throws MissingIdException
     */
    protected static function getId($path)
    {
        $id = current($path['args']);
        if (empty($id) || !is_numeric($id)) {
            throw new MissingIdException('Missing id for addresses');
        }
        return $id;
    }

    /**
     * Gets a single address.
     *
     * @param int $id Identifier for the address you want.
     *
     * @return array The address if it exists, false otherwise.
     */
    public function getAddress($id)
    {
        if ($address = $this->model->getAddress($id)) {
            return self::returnSuccess($address);
        } else {
            return self::returnError('Not Found');
        }
    }

    /**
     * @param $data
     */
    public function createAddress($data)
    {
        if ($return = $this->model->createAddress($data)) {
            return self::returnSuccess('Created successfully');
        } else {
            // We won't get into specifics.
            // So all errors are Internal errors.
            return self::returnError('Internal error');
        }
    }

    public function updateAddress($id, $data)
    {
        if ($return = $this->model->updateAddress($id, $data)) {
            return self::returnSuccess('Updated successfully');
        } else {
            // We won't get into specifics.
            // So all errors are Internal errors.
            return self::returnError('Internal error');
        }
    }

    public static function validateInput($data)
    {
        $fields = array('name', 'phone', 'street');
        foreach ($fields as $field) {
            // We want values for every field.
            // We're not adding further validation
            // since we don't have info for doing that,
            // but we could easily do it via regexp.
            if (empty($data[$field])) {
                return false;
            }
        }
        return true;
    }


    public function deleteAddress($id)
    {
        if ($result = $this->model->deleteAddress($id)) {
            return self::returnSuccess('Deleted successfully');
        } else {
            // We won't get into specifics,
            // since we're not using authentication at this moment.
            // So errors are Internal errors.
            return self::returnError('Internal error');
        }
    }
}

