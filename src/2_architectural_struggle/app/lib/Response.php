<?php
/**
 * @file
 * Controller class.
 * Parent class for all controllers in this app.
 *
 * @author Modesto Caballero <hola@modestocaballero.com>
 *
 * PHP Version 5.3.0
 */

namespace app\lib;

/**
 * Class Response
 *
 * Helps Controllers manage responses.
 */
class Response
{
    protected $data;
    protected $code;
    protected $errorMessage;

    /**
     * Setter for $data.
     *
     * @param $data
     */
    function setData($data)
    {
        $this->data = $data;
    }

    /**
     * Setter for code.
     *
     * @param $code
     */
    function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * Getter for $code.
     */
    function getCode()
    {
        return $this->code;
    }

    /**
     * Setter for $errorMessage.
     *
     * @param string $message
     */
    function setErrorMessage($errorMessage = '') {
        $this->errorMessage = $errorMessage;
    }

    /**
     * Getter for $errorMessage.
     */
    function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * Easy way to set status code and error
     * message depending on wether it's successful or not.
     *
     * @param bool $isSuccessful Is the response successful?
     * @param string $errorMessage Error message if needed
     *  (if it has been unsuccesful).
     */
    function setSuccessful($isSuccessful = true, $errorMessage = '')
    {
        if ($isSuccessful) {
            // code 200: OK.
            $this->setCode(200);
            $this->setErrorMessage();
        } else {
            $this->setErrorMessage($errorMessage);
            switch ($errorMessage) {
            case 'Not Found':
                $this->setCode(404);
                break;
            case 'ID is Missing or Wrong':
            case 'Data is empty or not valid.':
                $this->setCode(400);
                break;
            // Internal error will be also the fallback.
            case 'Internal error':
            default:
                $this->setCode(500);
                break;
            }
        }
    }

    /**
     * Returns response as an array.
     *
     * @return array
     */
    function toArray()
    {
        $output = array(
            'code' => $this->code,
            'error' => $this->errorMessage,
            'data' => $this->data,
        );
        // filter out empty entries:
        // e.g we don't get error key on a successful result,
        return array_filter(
            $output,
            function ($entry) {
                return !empty($entry);
            }
        );
    }

    /**
     * Returns response as JSON.
     *
     * @return string JSON representation of current response.
     */
    function toJSON()
    {
        $headerMessage = '';
        switch ($this->getCode()) {
        case 200:
            $headerMessage = "200 OK";
            break;
        case 201:
            $headerMessage = "201 Created";
            break;
        case 400:
            $headerMessage = "400 Bad Request";
            break;
        case 404:
            $headerMessage = "404 Not Found";
            break;
        case 405:
            $headerMessage = "405 Method Not Allowed";
            break;
        }
        header("HTTP/1.1 {$headerMessage}");
        return json_encode($this->toArray());
    }
}