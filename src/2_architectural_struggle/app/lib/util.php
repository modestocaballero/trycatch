<?php
/**
 * @file
 * Functions for utilities for this app.
 */

/**
 * Magic function __autoload
 * Provides autoload for classes according to PSR-0.
 *
 * @param string $className Name for the class to be loaded.
 *
 * @see http://www.php-fig.org/psr/psr-0/
 *
 */
function __autoload($className)
{
    $className = ltrim($className, '\\');
    $fileName  = '';
    $namespace = '';
    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }
    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

    require $fileName;
}