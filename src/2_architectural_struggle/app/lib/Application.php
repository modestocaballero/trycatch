<?php
/**
 * @file
 * Class Application
 * Includes url parsing and routing.
 *
 * @author Modesto Caballero <hola@modestocaballero.com>
 *
 * PHP Version 5.3.0
 */

namespace app\lib;
use app\controller\AddressController;
use app\controller\Controller;
use app\model\AddressModel;

/**
 * Class Application
 * @package app\lib
 */
class Application {
    /**
     * Constructor for Application class.
     */
    function __construct()
    {
        // URLs are now like trycatch.local/operation/arg1/arg2
        $path = Controller::urlParse();

        switch ($path['noun']) {
        case 'addresses':
            $storage = \app\model\Storage::getInstance(
                'mysql',
                array(
                    'host' => 'localhost',
                    'db' => 'trycatch',
                    'user' => 'root',
                    'pass' => '',
                    'table' => 'addresses',
                )
            );
            $model = new AddressModel($storage);
            $controller = new AddressController($model);
            $controller->start();
            break;
        default:
            // Currently we only handle resources under 'clients'
            header('HTTP/1.1 404 Not Found');
        }
    }


}