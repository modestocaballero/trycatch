<?php

require_once ('01_starter/1A_multiples/Multiplier.php');
require_once ('01_starter/1A_multiples/Solver.php');
require_once ('01_starter/1B_power/Math.php');
require_once ('01_starter/1C_fibonacci/Fibonacci.php');
require_once ('2_architectural_struggle/app/lib/Response.php');
require_once ('2_architectural_struggle/app/model/Storage.php');
require_once ('2_architectural_struggle/app/model/StorageMySQL.php');
require_once ('2_architectural_struggle/app/model/StorageCSV.php');
require_once ('2_architectural_struggle/app/model/Model.php');
require_once ('2_architectural_struggle/app/model/AddressModel.php');
require_once ('2_architectural_struggle/app/controller/Controller.php');
require_once ('2_architectural_struggle/app/controller/AddressController.php');