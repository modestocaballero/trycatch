<?php
/**
 * Tests for Solver for Multiplier class.
 */

namespace multiples;

class SolverTest extends \PHPUnit_Framework_TestCase {
  private $solver;

  public function setUp() {
    $this->solver = new Solver();
  }

  /**
   * Tests sumMultiplesUnderMax() function.
   */
  public function testSumMultiplesUnderMax() {
    $this->assertEquals($this->solver->sumMultiplesUnderMax(array(3,5),10), 23);
    $this->assertEquals($this->solver->sumMultiplesUnderMax(array(3,5),20), 93);
    $this->assertEquals($this->solver->sumMultiplesUnderMax(array(3,5),1000), 266333);
  }

  /**
   * Tests sumMultiplesUnderMaxFaster() function.
   */
  public function testSumMultiplesUnderMaxFaster() {
    $this->assertEquals($this->solver->sumMultiplesUnderMaxFaster(array(3,5),10), 23);
    $this->assertEquals($this->solver->sumMultiplesUnderMaxFaster(array(3,5),20), 93);
    $this->assertEquals($this->solver->sumMultiplesUnderMaxFaster(array(3,5),1000), 266333);
  }

  /**
   * Tests sumCommonMultiplesUnderMax() function.
   */
  function testSumCommonMultiplesUnderMax() {
    $this->assertEquals($this->solver->sumCommonMultiplesUnderMax(array(3,5),20), 15);
    $this->assertEquals($this->solver->sumCommonMultiplesUnderMax(array(3,5),35), 45);
    $this->assertEquals($this->solver->sumCommonMultiplesUnderMax(array(3,4),1000), 41832);
  }


  /**
   * Tests sumMultiplesOneValueUnderMax() function.
   */
  public function testSumMultiplesOneValueUnderMax() {
    $this->assertEquals($this->solver->sumMultiplesOneValueUnderMax(3,10), 18);
  }

  /**
   * Tests commonMultiple() function.
   */
  public function testArrayMultiply() {
    $this->assertEquals($this->solver->commonMultiple(array(2)), 2);
    $this->assertEquals($this->solver->commonMultiple(array(1,2,3)), 6);
    $this->assertEquals($this->solver->commonMultiple(array(1,2,2,3)), 6);
  }
}
 