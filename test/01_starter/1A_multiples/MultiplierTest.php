<?php

/**
 * Tests for Multiplier class.
 */

namespace multiples;

class MultiplierTest extends \PHPUnit_Framework_TestCase {
  // will contain a Multiplier with value 3.
  private $three;
  // will contain a Multiplier with value 5.
  private $five;

  public function setUp() {
    $this->five = new Multiplier(5);
    $this->three = new Multiplier(3);
  }

  /**
   * Tests value() function.
   */
  public function testvalue() {
    $this->assertEquals($this->five->value(), 5);
    $this->assertEquals($this->three->value(), 3);
  }

  /**
   * Tests currentMultiple() function.
   */
  public function testCurrentMultiple() {
    $this->assertEquals($this->five->currentMultiple(), 5);
    $this->assertEquals($this->three->currentMultiple(), 3);
  }

  /**
   * Tests next() function.
   */
  public function testNext() {
    $this->three->next();
    $this->five->next();
    $this->assertEquals($this->five->currentMultiple(), 10);
    $this->assertEquals($this->three->currentMultiple(), 6);
    $this->three->next();
    $this->five->next();
    $this->assertEquals($this->five->currentMultiple(), 15);
    $this->assertEquals($this->three->currentMultiple(), 9);
  }

  /**
   * Tests prev() function
   */
  public function testPrev() {
    $this->assertEquals($this->three->currentMultiple(), 3);
    $this->three->prev();
    $this->assertEquals($this->three->currentMultiple(), 3);
    $this->three->next();
    $this->three->next();
    $this->assertEquals($this->three->currentMultiple(), 9);
    $this->three->prev();
    $this->assertEquals($this->three->currentMultiple(), 6);
  }

  /**
   * Tests highestMultipleUnder() function.
   */
  public function testHighestMultipleUnder() {
    $this->assertEquals($this->five->highestMultipleUnder(100), 95);
    $this->assertEquals($this->three->highestMultipleUnder(100), 99);
  }
}
 