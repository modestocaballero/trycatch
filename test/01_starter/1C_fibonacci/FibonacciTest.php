<?php
/**
 * Tests for Fibonacci class.
 */

namespace fibonacci;

class FibonacciTest extends \PHPUnit_Framework_TestCase {
  public function testFibonacciNonRecursive() {
    $this->assertEquals(Fibonacci::fibonacciNonRecursive(1), array(1));
    $this->assertEquals(Fibonacci::fibonacciNonRecursive(2), array(1,1));
    $this->assertEquals(Fibonacci::fibonacciNonRecursive(5), array(1,1,2,3,5));
    $this->assertEquals(Fibonacci::fibonacciNonRecursive(10), array(1,1,2,3,5,8,13,21,34,55));
  }
  public function testFibonacciRecursive() {
    $this->assertEquals(Fibonacci::fibonacciRecursive(1), array(1));
    $this->assertEquals(Fibonacci::fibonacciRecursive(2), array(1,1));
    $this->assertEquals(Fibonacci::fibonacciRecursive(5), array(1,1,2,3,5));
    $this->assertEquals(Fibonacci::fibonacciRecursive(10), array(1,1,2,3,5,8,13,21,34,55));
  }
}
 