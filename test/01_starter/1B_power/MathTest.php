<?php
/**
 * Tests for Match class.
 */

namespace power;

class MathTest extends \PHPUnit_Framework_TestCase {
  private $math;

  public function setUp() {
    $this->math = new Math();
  }

  /**
   * Tests multiply() function.
   */
  public function testMultiply() {
    $this->assertEquals($this->math->multiply(4,7),28);
    $this->assertEquals($this->math->multiply(5,3),15);
    $this->assertEquals($this->math->multiply(3,5),15);
  }
  /**
   * Tests power() function.
   */
  public function testPower() {
    $this->assertEquals($this->math->power(10,0),1);
    $this->assertEquals($this->math->power(10,1),10);
    $this->assertEquals($this->math->power(10,2),100);
    $this->assertEquals($this->math->power(4,3),64);
  }
}
 