<?php

use Behat\MinkExtension\Context\MinkContext;

/**
 * Features context.
 */
class FeatureContext extends MinkContext
{
  /**
   * @Given /^I am on address "([^"]*)"$/
   */
  public function iAmOnAddress($id)
  {
    $page = "/addresses/" . $id;
    $this->visit($page);
  }
}