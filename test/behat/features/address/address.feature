Feature:
  If I ask for different addresses,
  I get the expected info.

  Scenario Outline: See address detail
    Given I am on address <id>
    Then I should see <street>
  Examples:
    | id | street |
    | "0" | "Michalowskiego 41" |
    | "1" | "Opata Rybickiego 1" |
    | "2" | "Horacego 23" |