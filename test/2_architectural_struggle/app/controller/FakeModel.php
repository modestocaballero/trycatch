<?php
/**
 * @file
 * Class FakeModel
 * Provides a Model subclass for being used
 * as a parameter on constructors on testing.
 *
 * @author Modesto Caballero <hola@modestocaballero.com>
 *
 * PHP Version 5.3.0
 */

/**
 * Class FakeModel
 * Provides a Model subclass for being used
 * as a parameter on constructors on testing.
 */
class FakeModel extends app\model\Model
{
    public function getAddress($id) {
        $result = false;
        switch ($id) {
        case 1:
            $result = array(
                'name' => 'Test name',
                'phone' => '666666',
                'street' => 'Test st, 45',
            );
            break;
        }
        return $result;
    }

    public function createAddress($data)
    {
        if (empty($data)) {
            return false;
        }
        return true;
    }

    public function updateAddress($data)
    {
        if (empty($data)) {
            return false;
        }
        return true;
    }

    public function deleteAddress($id)
    {
        $result = false;
        switch ($id) {
        case 1:
            $result = true;
        }
        return $result;
    }
}