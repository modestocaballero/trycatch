<?php
/**
 * @file
 * Class AddressControllerTest
 * Tests for AddressController.
 *
 * @author Modesto Caballero <hola@modestocaballero.com>
 *
 * PHP Version 5.3.0
 */


require_once 'FakeModel.php';

use \app\controller\Controller;
use \app\controller\AddressController;

/**
 * Class AddressControllerTest
 * Tests for AddressController.
 */
class AddressControllerTest extends PHPUnit_Framework_TestCase {
    protected  $controller;

    function setUp() {
        $model = new FakeModel();
        $this->controller = new AddressController($model);
        $_SERVER['CONTENT_TYPE'] = "application/json; charset=UTF-8";
    }
    /**
     * @runInSeparateProcess
     *
     * Tests getAddress() function.
     */
    function testGetAddress()
    {
        // Testing existing address.
        $result = Controller::decodeInput($this->controller->getAddress(1));
        // Return no error code.
        $this->assertArrayNotHasKey('error', $result);
        // status code: OK.
        $this->assertEquals($result['code'], 200);
        // retrieving first element and checking it has all the expected data.
        $data = $result['data'];
        $this->assertNotEmpty($data->name);
        $this->assertNotEmpty($data->phone);
        $this->assertNotEmpty($data->street);

        // Testing non-existing address.
        $result = Controller::decodeInput($this->controller->getAddress(7));
        $this->assertNotEmpty($result['error']);
        // status code: Not Found.
        $this->assertEquals($result['code'], 404);
    }

    /**
     * Tests createAddress() function.
     */
    function testCreateAddress()
    {
        $rightData = array(
            'name' => 'Name 1',
            'phone' => '6666666',
            'street' => 'any street 1',
        );

        // Testing existing address.
        $result = Controller::decodeInput($this->controller->createAddress($rightData));
        // Return no error code.
        $this->assertArrayNotHasKey('error', $result);
        // status code: OK.
        $this->assertEquals($result['code'], 200);

        // If there were validation errors,
        // an error would be raised before entering createAddress()
        // so we're not testing that here.
    }

    /**
     * Tests updateAddress() function.
     */
    function testUpdateAddress()
    {
        $data = array(
            'name' => 'Name 1',
            'phone' => '6666666',
            'street' => 'any street 1',
        );

        // Testing existing address.
        $result = Controller::decodeInput($this->controller->updateAddress(1, $data));
        // Return no error code.
        $this->assertArrayNotHasKey('error', $result);
        // status code: OK.
        $this->assertEquals($result['code'], 200);

        // If there were validation errors,
        // an error would be raised before entering createAddress()
        // so we're not testing that here.
    }

    public function testDeleteAddress()
    {
        // Existing address, successful.
        $result = Controller::decodeInput($this->controller->deleteAddress(1));
        // Return no error code.
        $this->assertArrayNotHasKey('error', $result);
        // status code: OK.
        $this->assertEquals($result['code'], 200);


        // Non-existing address, error.
        $result = Controller::decodeInput($this->controller->deleteAddress(7));

        $this->assertNotEmpty($result['error']);
        // status code: Not Found.
        $this->assertEquals($result['code'], 500);
    }

    /**
     *
     */
    function testValidateInput()
    {
        // data ok, all items have value.
        $data1 = array(
            'name' => 'name1',
            'phone' => 'phone1',
            'street' => 'street 45',
        );

        $this->assertTrue(AddressController::validateInput($data1));

        // data ko, an item is empty.
        $data2 = array(
            'name' => '',
            'phone' => 'phone1',
            'street' => 'street 45',
        );

        $this->assertFalse(AddressController::validateInput($data2));

        // data ko, an item is missing.
        $data3 = array(
            'phone' => 'phone1',
            'street' => 'street 45',
        );

        $this->assertFalse(AddressController::validateInput($data3));
    }

}