<?php
/**
 * @file
 * Class StorageCSVTest
 * Provides tests for StorageCSV class.
 *
 * @author Modesto Caballero <hola@modestocaballero.com>
 *
 * PHP Version 5.3.0
 */

/**
 * Class StorageCSVTest
 * Provides tests for StorageCSV class.
 */
class StorageCSVTest extends PHPUnit_Framework_TestCase {

    /**
     * Tests function load().
     */
    function testLoad()
    {
        $storage = \app\model\Storage::getInstance(
            'csv',
            array(
                dirname(__FILE__) . '/testAddress.csv',
                array('name', 'phone', 'street'),
            )
        );

        // existing entry, with headers.
        $result = $storage->load(1);
        $this->assertNotEmpty($result['name']);
        $this->assertNotEmpty($result['phone']);
        $this->assertNotEmpty($result['street']);

        // constructor with no header, and no clear
        // it should work as previous one,
        // since instance is reused if we don't clear it
        $storage = \app\model\Storage::getInstance(
            'csv',
            array(
                dirname(__FILE__) . '/testAddress.csv',
            )
        );

        // existing entry, with headers.
        $result = $storage->load(1);
        $this->assertNotEmpty($result['name']);
        $this->assertNotEmpty($result['phone']);
        $this->assertNotEmpty($result['street']);

        // constructor with no header, and clear
        // (it should clear instance in memory
        // and create a new one, with no headers)
        $storage = \app\model\Storage::getInstance(
            'csv',
            array(
                dirname(__FILE__) . '/testAddress.csv',
            ),
            true
        );

        // existing entry, without headers.
        $result = $storage->load(1);
        $this->assertNotEmpty($result);
        $this->assertArrayNotHasKey('name', $result);
        $this->assertArrayNotHasKey('phone', $result);
        $this->assertArrayNotHasKey('street', $result);
        // it has numeric headers.
        $this->assertArrayHasKey(0, $result);
        $this->assertArrayHasKey(1, $result);
        $this->assertArrayHasKey(2, $result);

    }
}
 