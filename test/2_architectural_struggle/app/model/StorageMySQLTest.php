<?php
/**
 * @file
 * Class StorageMySQLTest
 * Provides tests for StorageMySQL class.
 *
 * @author Modesto Caballero <hola@modestocaballero.com>
 *
 * PHP Version 5.3.0
 */

/**
 * Class StorageMySQLTest
 * Provides tests for StorageMySQL class.
 */
class StorageMySQLTest extends PHPUnit_Framework_TestCase {
    protected $storage;

    /**
     * Tests constructor.
     */
    function testConstruct()
    {
        // Missing args, should raise an exception.
        try {
            $wrongStorage = \app\model\Storage::getInstance(
                'mysql',
                array(
                    'host' => 'localhost',
                    'db' => 'trycatchtest',
                    'pass' => 'anypass',
                )
            );
            $this->fail("Expected exception not thrown");
        } catch(Exception $e) {
            $this->assertEquals("StorageMySQL: missing user argument", $e->getMessage());
        }

        // Empty args, should raise an exception.
        try {
            $wrongStorage = \app\model\Storage::getInstance(
                'mysql',
                array(
                    'host' => 'localhost',
                    'db' => 'trycatchtest',
                    'user' => '',
                    'pass' => 'anypass',
                )
            );
            $this->fail("Expected exception not thrown");
        } catch(Exception $e) {
            $this->assertEquals("StorageMySQL: missing user argument", $e->getMessage());
        }

    }

    /**
     * Tests function load().
     */
    function testLoad()
    {
        $this->constructStorage();
        $storage = $this->storage;
        // existing entry.
        $result = $storage->load(1);
        $this->assertNotEmpty($result['name']);
        $this->assertNotEmpty($result['phone']);
        $this->assertNotEmpty($result['street']);

        // non-existing entry.
        $result = $storage->load(7);
        $this->assertFalse($result);
    }

    /**
     * Tests delete() function.
     */
    public function testDelete()
    {
        $this->constructStorage();
        $storage = $this->storage;
        // load existing entry.
        $result = $storage->load(2);
        $this->assertNotEmpty($result['name']);
        $this->assertNotEmpty($result['phone']);
        $this->assertNotEmpty($result['street']);
        // delete entry, check it isn't there anymore.
        $result = $storage->delete(2);
        // deletion is successful.
        $this->assertTrue($result);
        $result = $storage->load(2);
        $this->assertFalse($result);
        // delete it again, it's successful despite it isn't there:
        // that's the expected behavior.
        $result = $storage->delete(2);
        $this->assertTrue($result);
    }

    /**
     *
     */
    protected function constructStorage()
    {
        $this->storage = \app\model\Storage::getInstance(
            'mysql',
            array(
                'host' => 'localhost',
                'db' => 'trycatchtest',
                'user' => 'root',
                'pass' => '',
                'table' => 'addresses',
            )
        );

    }

}
