<?php
/**
 * @file
 * Class StorageTest
 * Provides tests for Storage class.
 *
 * @author Modesto Caballero <hola@modestocaballero.com>
 *
 * PHP Version 5.3.0
 */

/**
 * Class StorageTest
 * Provides tests for Storage class.
 */
class StorageTest extends PHPUnit_Framework_TestCase {
    /**
     * Tests getInstance() function.
     */
    function testGetInstance()
    {
        // Passing a non-existing type should raise an execption.
        try {
            $wrongStorage = \app\model\Storage::getInstance('nonExisting');
            $this->fail("Expected exception not thrown");
        } catch(Exception $e) {
            $this->assertEquals("Storage class doesn't exist.", $e->getMessage());
        }

    }

}
 