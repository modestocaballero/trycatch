<?php
/**
 * @file
 * Class AddressModelTest
 * Provides tests for AddressModel class.
 *
 * @author Modesto Caballero <hola@modestocaballero.com>
 *
 * PHP Version 5.3.0
 */

/**
 * Class AddressModelTest
 * Provides tests for AddressModel class.
 */
class AddressModelTest extends PHPUnit_Framework_TestCase
{
    protected $model;

    /**
     * Initializes mode. with a test csv.
     */
    function setUp()
    {
        $storage = \app\model\Storage::getInstance(
            'csv',
            array(
                dirname(__FILE__) . '/testAddress.csv',
                array('name', 'phone', 'street'),
            )
        );
        $this->model = new \app\model\AddressModel($storage);
    }

    /**
     * Tests getAddress() function.
     *
     * @fixme:
     * After creating Storage class, this one is pretty redundant,
     * so it will change or disappear.
     */
    function testGetAddress()
    {
        // existing address.
        $result = $this->model->getAddress(1);
        $this->assertNotEmpty($result['name']);
        $this->assertNotEmpty($result['phone']);
        $this->assertNotEmpty($result['street']);

        // non-existing address.
        $result = $this->model->getAddress(7);
        $this->assertFalse($result);
    }
}
 