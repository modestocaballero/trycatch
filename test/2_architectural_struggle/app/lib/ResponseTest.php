<?php
/**
 * Created by PhpStorm.
 * User: aguasingas
 * Date: 21/08/14
 * Time: 11:16
 */

class ResponseTest extends PHPUnit_Framework_TestCase {
    protected $response;
    function setUp() {
        $this->response = new \app\lib\Response();
    }

    /**
     * Tests setSuccessful() function.
     */
    function testIsSuccessful()
    {
        $response = $this->response;
        $response->setSuccessful();
        // It is successful: its code is 200, has no error message.
        $this->assertEquals($response->getCode(), 200);
        $this->assertEmpty($response->getErrorMessage());
    }
}
 